#!/bin/sh -eux
curl -X POST "https://api.cloudflare.com/client/v4/zones/7c06b35c2392935ebb0653eaf94a3e70/purge_cache" \
         -H "X-Auth-Email: $CLOUDFLARE_EMAIL" \
         -H "X-Auth-Key: $CLOUDFLARE_TOKEN" \
         -H "Content-Type: application/json" \
         --data '{"purge_everything":true}'
