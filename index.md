---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
#    <span class="date">{{ post.date }}</span>

layout: home
---
<!-- This loops through the paginated posts -->
{% for post in site.posts limit: site.paginate %}
<article class="post">
  <h1><a href="{{ post.url }}">{{ post.title }}</a></h1>
  <p class="post-meta">
    <time class="dt-published" datetime="{{ post.date | date_to_xmlschema }}" itemprop="datePublished">
      {%- assign date_format = site.minima.date_format | default: "%b %-d, %Y" -%}
      {{ post.date | date: date_format }}
    </time>
  </p>
  <div class="content">
    {{ post.content }}
  </div>
</article>
{% endfor %}

<!-- Pagination links -->
<div class="pagination">
  <a href="/page/2" class="next">Older posts</a>
</div>

